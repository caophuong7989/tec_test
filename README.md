## Prequisites
python3 and pip

## Installation and Usage
0. (Optional) Create your virtual env: `python3 -m venv tec_env` and activate it
1. Install all dependencies listed in `pip install -r requirements.txt`
2. Run `python app.py`
3. Run test by `cd test && pytest`