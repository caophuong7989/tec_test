import dash
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output, State
import plotly.graph_objects as go
from fetch_data import processed_data
import pandas as pd
import json
import pathlib
# Path
BASE_PATH = pathlib.Path(__file__).parent.resolve()
DATA_PATH = BASE_PATH.joinpath('data/input.csv').resolve()

"""INIT APP"""
app = dash.Dash(
    __name__,
    meta_tags=[{'name': 'viewport',
                'content': 'width=device-width, initial-scale=1'}],
)
app.title = 'tec'
server = app.server
app.config.suppress_callback_exceptions = True

"""FETCH DATA SESSION"""
# this is defaut assume value for maximun floor area, size of car and size of bike
max_floor_area = 1000
car_size = 6
bike_size = 2
DEFAULT_COLUMN_LIST = ['date', 'car_in', 'car_out',
                       'car_accumulation', 'bike_in',
                       'bike_out', 'bike_accumulation',
                       'avg_accumulation']

"""RENDER LAYOUT SESSION"""


def generate_header():
    return html.Div(
        id='control-card',
        children=[
            html.Div(
                className='control-child',
                children=[
                    html.P('Maximum Floor Area (m2)'),
                    dcc.Input(
                        id='floor-area',
                        type='number',
                        min=10,
                        max=10000,
                        value=max_floor_area,
                        placeholder='Max floor area (m2)',
                    ),
                ]
            ),
            html.Div(
                className='control-child',
                children=[
                    html.P('Assumed Car Size (m2)'),
                    dcc.Input(
                        id='car-size',
                        type='number',
                        min=1,
                        max=20,
                        value=car_size,
                        placeholder='Car Size (m2)',
                    ),
                ]
            ),
            html.Div(
                className='control-child',
                children=[
                    html.P('Assumed Bike Size (m2)'),
                    dcc.Input(
                        id='bike-size',
                        type='number',
                        min=1,
                        max=10,
                        value=bike_size,
                        placeholder='Bike Size (m2)',
                    ),
                ]
            ),
            html.Div(
                className='result-child bg-light',
                children=[
                    html.H1(id='result-area', children=['N/A']),
                    html.P('Gross Floor Area (m2)'),
                ]
            ),
            html.Div(
                className='result-child bg-dark text-white',
                children=[
                    html.H1(id='result-level', children=['N/A']),
                    html.P('Number of levels'),
                ]
            ),
        ],
    )


def description_card():
    return html.Div(
        children=[
            html.H5('Vence garage analyze'),
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Li(
                                children=[
                                    'With a In-out statistic in day interval, we can calculate Accumulation of each vehicle types']
                            ),
                            html.Li(
                                children=[
                                    'Accumulation = Initial vehicle count + vehicle in - vehicle out.']
                            ),
                            html.Li(
                                children=[
                                    'Due to lacking of historical data, accumulation might be negative, so I will assume min of accumulation = 0, then shift all accumulation with absolute of negative min value.']
                            ),
                            html.Li(
                                children=[
                                    'Average Accumulation = (car_accumulation*assumed_car_size + bike_accumulation*assumed_bike_size)/average_vehicle_size']
                            ),
                            html.Li(
                                children=[
                                    'Note: max_area=10000m2, max_car_size=20m2, max_bike_size=10m2']
                            ),
                        ]
                    )
                ]
            ),
        ],
    )


app.layout = html.Div(
    id='app-container',
    children=[
        dcc.Store(id='session', storage_type='session'),
        # Header
        html.Div(
            id='header-card',
            children=[generate_header()]
        ),
        html.Div(
            id='left-column',
            className='three columns',
            children=[description_card()]
        ),
        # Right column
        html.Div(
            id='main-column',
            className='nine columns',
            children=[
                html.Div(
                    className='chart-child',
                    children=[
                        html.P('Accumulation Chart'),
                        dcc.Graph(id='line-chart-card'),
                    ],
                ),
                html.Div(
                    className='chart-child',
                    children=[
                        html.P('Dataset'),
                        dash_table.DataTable(
                            id='table-card',
                            columns=[{'name': i, 'id': i}
                                     for i in DEFAULT_COLUMN_LIST],
                            page_current=0,
                            page_size=10,
                            page_action='custom',
                            sort_action='custom',
                            sort_mode='single',
                            sort_by=[]
                        )
                    ],
                )
            ],
        )
    ],
)

""" CALLBACK SESSION"""


@app.callback(
    Output('session', 'data'),
    Input('floor-area', 'value'),
    Input('car-size', 'value'),
    Input('bike-size', 'value'),
    State('session', 'data')
)
def on_change_input(floor_area, car_size, bike_size, data):
    if floor_area is None or car_size is None or bike_size is None:
        raise PreventUpdate
    current_data = processed_data(DATA_PATH, floor_area, car_size, bike_size)
    data = {
        'df': current_data['df'].to_json(orient='records'),
        'demanding_area': current_data['demanding_area'],
        'demanding_level': current_data['demanding_level']
    }
    return data


@app.callback(
    Output('result-area', 'children'),
    Output('result-level', 'children'),
    Input('session', 'data'))
def update_result_card(data):
    data = data or {}
    return data.get('demanding_area', 0), data.get('demanding_level', 0)


@app.callback(Output('line-chart-card', 'figure'),
              Input('session', 'data'))
def update_graph(data):
    current_df = pd.DataFrame(json.loads(data.get('df', [])))
    recommend_df = current_df[current_df['avg_accumulation']
                              == current_df['avg_accumulation'].max()]

    layout = go.Layout(xaxis={'type': 'date',
                              'tick0': current_df['date'][0],
                              'tickmode': 'linear',
                              'dtick': 86400000.0 * 15})
    fig = go.Figure(
        data=[
            go.Scatter(
                mode='lines',
                name='car',
                x=current_df['date'],
                y=current_df['car_accumulation'],
            ),
            go.Scatter(
                mode='lines',
                name='bike',
                x=current_df['date'],
                y=current_df['bike_accumulation'],
            ),
            go.Scatter(
                mode='lines',
                name='average',
                x=current_df['date'],
                y=current_df['avg_accumulation'],
            ),
        ],
        layout=layout
    )
    fig.add_hline(
        y=recommend_df['avg_accumulation'].values[0],
        line_dash='dot',
        row=3,
        col='all',
        annotation_text='Recommend {} car and {} bike bays'.format(
            recommend_df['car_accumulation'].values[0],
            recommend_df['bike_accumulation'].values[0]),
        annotation_position='bottom right'
    )
    return fig


@ app.callback(
    Output('table-card', 'data'),
    Input('table-card', 'page_current'),
    Input('table-card', 'page_size'),
    Input('table-card', 'sort_by'),
    Input('session', 'data'))
def update_table(page_current, page_size, sort_by, data):
    current_df = pd.DataFrame(json.loads(data.get('df', [])))
    current_df['timestamp'] = current_df['date']
    current_df['date'] = pd.to_datetime(
        current_df['date'], unit='ms').dt.strftime('%d-%m-%Y')
    if len(sort_by):
        if sort_by[0]['column_id'] == 'date':
            sort_by[0]['column_id'] = 'timestamp'
        dff = current_df.sort_values(
            sort_by[0]['column_id'],
            ascending=sort_by[0]['direction'] == 'asc',
            inplace=False
        )
    else:
        dff = current_df

    return dff.iloc[
        page_current*page_size:(page_current + 1)*page_size
    ].to_dict('records')


# Run the server
if __name__ == "__main__":
    app.run_server(debug=False)
