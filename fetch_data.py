# import numpy as np
import pandas as pd
import numpy as np
import math


def processed_data(path, max_floor_area, car_size, bike_size):
    df = pd.read_csv(path,
                     dtype={'date': 'str', 'car_in': 'int64', 'car_out': 'int64',
                            'bike_in': 'int64', 'bike_out': 'int64'},
                     names=['date', 'car_in', 'car_out',
                            'bike_in', 'bike_out'],
                     header=0)
    df['date'] = pd.to_datetime(df['date'], format='%d/%m/%Y', errors='coerce')
    df['car_accumulation'] = (df['car_in'] - df['car_out']
                              ).cumsum().shift(0).ffill()
    df['bike_accumulation'] = (
        df['bike_in'] - df['bike_out']).cumsum().shift(0).ffill()

    # accumulation can be negative due to lack of historical data
    # in this case, assume min of accumulation = 0, then we have to shift all accumulation with min negative value
    if df['car_accumulation'].min() < 0:
        df['car_accumulation'] = df['car_accumulation'] + \
            abs(df['car_accumulation'].min())
    if df['bike_accumulation'].min() < 0:
        df['bike_accumulation'] = df['bike_accumulation'] + \
            abs(df['bike_accumulation'].min())

    avg_size = (car_size + bike_size)/2
    df['avg_accumulation'] = np.ceil((
        df['car_accumulation'] * car_size + df['bike_accumulation']*bike_size)/avg_size).astype('int64')

    # calculate result
    demanding_area = 0
    demanding_level = 0
    if max_floor_area > 0:
        demanding_area = math.ceil(df['avg_accumulation'].max() * avg_size)
        demanding_level = math.ceil(demanding_area/max_floor_area)
    return {
        'df': df[['date', 'car_in', 'car_out', 'car_accumulation', 'bike_in', 'bike_out', 'bike_accumulation', 'avg_accumulation']],
        'demanding_area': demanding_area,
        'demanding_level': demanding_level,
    }
