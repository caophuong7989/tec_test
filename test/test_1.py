from fetch_data import processed_data
from pandas._testing import assert_frame_equal
import pandas as pd
import pathlib
BASE_PATH = pathlib.Path(__file__).parent.resolve()
TEST_DATA_PATH = BASE_PATH.joinpath('data/input.csv').resolve()


def test_fetch_data():
    car_size = 4
    bike_size = 1
    max_area = 20
    expected_data = {
        'df': [
            {
                'date': 1577836800000,
                'car_in': 10,
                'car_out': 5,
                'car_accumulation': 5,
                'bike_in': 10,
                'bike_out': 7,
                'bike_accumulation': 3,
                'avg_accumulation': 10
            },
            {
                'date': 1577923200000,
                'car_in': 10,
                'car_out': 12,
                'car_accumulation': 3,
                'bike_in': 5,
                'bike_out': 8,
                'bike_accumulation': 0,
                'avg_accumulation': 5
            },
            {
                'date': 1578009600000,
                'car_in': 10,
                'car_out': 0,
                'car_accumulation': 13,
                'bike_in': 20,
                'bike_out': 5,
                'bike_accumulation': 15,
                'avg_accumulation': 27
            }
        ],
        'demanding_area': 68,
        'demanding_level': 4
    }
    test_data = processed_data(TEST_DATA_PATH, max_area, car_size, bike_size)
    test_df = pd.DataFrame(test_data['df'])
    expected_df = pd.DataFrame(expected_data['df'], dtype='int64')
    expected_df['date'] = pd.to_datetime(expected_df['date'], unit='ms')

    assert_frame_equal(test_df, expected_df)
    assert test_data['demanding_area'] == expected_data['demanding_area']
    assert test_data['demanding_level'] == expected_data['demanding_level']
